package view;

import core.MemoryMonitor;
import core.WindowsInfo;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;

import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.hyperic.sigar.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Клас для створення головного вікна програми
 * Created by Vlad on 10.12.2014.
 * @author Vlad
 */
public class Diagram  extends Application{
    /**
     * значення заггруженості процесора
     */
    private SimpleDoubleProperty processorValue;
    /**
     * силка на об*кт з якого можна
     * дізнатись завантаженість
     * процесора
     */
    private  Sigar sigar;
    /**
     * головна панель програми
     */
    private HBox mainBox;
    /**
     * сцена головного вікна
     */
    private Scene scene;

    /**
     * конструктор класу
     */
    public Diagram(){
        sigar = new Sigar();
        processorValue =  new SimpleDoubleProperty(20);
    }

    /**
     * метод знаходить інформацію про модель
     * та інші характеристики процесора
     * @return s характеристика процесора
     */
    private String cpuInfo() {
        String s = "";
        Sigar session = new Sigar();
        try {
            CpuInfo[] cpuInfoArray = session.getCpuInfoList();
            System.out.println("CPU MHz: "+cpuInfoArray[0].getMhz());
            System.out.println("CPU Model "+cpuInfoArray[0].getModel());
            System.out.println("CPU total cores "+ cpuInfoArray[0].getTotalCores());
            s+=cpuInfoArray[0].getModel();
                    //+" "+cpuInfoArray[0].getTotalCores()+" cores";
        } catch (SigarException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * метод повертає значення занятість процесора в %
     * @return занятість процесора
     * @throws SigarException
     */
    private double getMetric() throws SigarException {
        CpuPerc cpu = sigar.getCpuPerc();
        double system = cpu.getSys();
        double user = cpu.getUser();
        double s = cpu.getCombined();
        return s/2;
    }

    /**
     * метод для знаходження значення занятості в процесорі
     * для конкретного процеса
     * @param pid ІД процеса
     * @return значення в % заанятості цим процесом просесора
     * @throws SigarException
     */
    private double getMetric(String pid) throws SigarException {
        ProcCpu cpu = sigar.getProcCpu(pid);
        return cpu.getPercent();
    }

    /**
     * метод для створення головного вікна програми
     * @param primaryStage силка на об*єкт головного вікна програми
     */
    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        scene = new Scene(root, 700, 600, Color.WHITE);

        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("", processorValue.doubleValue() ),
                        new PieChart.Data("",100 - processorValue.doubleValue()));


        final PieChart pieChart = new PieChart(pieChartData);
        pieChartData.get(0).pieValueProperty().bind(processorValue);
        pieChart.prefHeight(200);
        pieChart.prefWidth(200);
        pieChart.maxWidth(200);
        pieChart.maxHeight(200);
        pieChart.legendVisibleProperty().setValue(false);
        pieChart.setStartAngle(180);
        pieChart.labelsVisibleProperty().setValue(false);
        applyCustomColorSequence(
                pieChartData,
                "goldenrod",
                "aqua"
        );
        mainBox = new HBox();
        mainBox.setSpacing(10);
        mainBox.setPadding(new Insets(5, 5, 5, 5));
        mainBox.prefHeightProperty().bind(scene.heightProperty());
        mainBox.prefWidthProperty().bind(scene.widthProperty());
        Label cpuINFO = new Label(cpuInfo());

        VBox leftBox = new VBox();
        leftBox.setSpacing(10);
        leftBox.setPadding(new Insets(5,5,5,5));
        leftBox.setStyle("-fx-border-radius: 5;" +
                         "-fx-border-color: darkgoldenrod;" +
                         "-fx-border-width: 3;");

        VBox buf = new VBox();
        SimpleStringProperty cpuGHZ = new SimpleStringProperty();
        Label l = new Label(" GHZ");
        l.textProperty().bind(cpuGHZ);

        buf.getChildren().addAll(pieChart,l);
        buf.setMaxHeight(180);
        buf.setMaxWidth(220);
        buf.setAlignment(Pos.TOP_CENTER);

        ProgressBar progressBar = new ProgressBar();
        progressBar.prefWidthProperty().bind(buf.widthProperty().subtract(40));
        SimpleDoubleProperty ramValue = new SimpleDoubleProperty(4000.0);
        SimpleStringProperty sRam = new SimpleStringProperty("");
        Label lRam = new Label("RAM used "+" GB of " + new MemoryMonitor().getRAMSize()+" GB");
        lRam.textProperty().bind(sRam);
        final CountThread countThread = new CountThread(ramValue,sRam);
        progressBar.progressProperty().bind(ramValue);

        VBox vBox = new VBox();
        vBox.setSpacing(5);
        vBox.setAlignment(Pos.BASELINE_CENTER);
        vBox.prefWidthProperty().bind(leftBox.widthProperty());
        vBox.getChildren().addAll(lRam,progressBar);

        VBox bufBox = new VBox();
        bufBox.setPadding(new Insets(5));
        bufBox.setSpacing(10);
        bufBox.setAlignment(Pos.BASELINE_CENTER);
        bufBox.prefWidthProperty().bind(leftBox.widthProperty());
        Label hd = new Label("Disks:");
        hd.setStyle("-fx-text-fill: blueviolet;");
        hd.setFont(new Font("Tahoma",17));
        bufBox.getChildren().add(hd);

        HashMap<String,String> map = new WindowsInfo().getWindowsInfo();
        VBox winBox = new VBox();
        winBox.setAlignment(Pos.TOP_CENTER);
        winBox.setSpacing(10);
        winBox.setPadding(new Insets(5));
        Label opsyInfo = new Label("About Operating system:");
        opsyInfo.setFont(new Font("Tahoma",15));
        opsyInfo.setStyle("-fx-text-fill: blueviolet;");
        Label win = new Label(map.get("os"));
        win.setFont(new Font("Tahoma",15));
        Label ar = new Label(map.get("arch"));
        ar.setFont(new Font("Tahoma",15));
        winBox.getChildren().addAll(opsyInfo,win,ar);
        leftBox.getChildren().addAll(cpuINFO, buf, vBox, bufBox,
                new MemoryView().getBox(),winBox);
        leftBox.setMaxWidth(320);
        leftBox.setMinWidth(220);

        VBox rightBox = new TablePanel().getBox(scene);
       // rightBox.setStyle("-fx-background-color: chocolate;");
        rightBox.prefWidthProperty().bind(scene.widthProperty());
        rightBox.prefHeightProperty().bind(scene.heightProperty());

        mainBox.getChildren().addAll(leftBox,rightBox);
        root.getChildren().add(mainBox);

        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("Task Manager by Rozkudanuy Vlad");
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
        new Thread(new Changer(pieChart,cpuGHZ)).start();
        new Thread(countThread).start();
        new Thread(new Refresher(root,mainBox)).start();
    }

    /**
     * метод для зміни кольору секцій діаграми занятості
     * процесора
     * @param pieChartData діаграма
     * @param pieColors нові кольори секцій
     */
    private void applyCustomColorSequence(ObservableList<PieChart.Data> pieChartData, String... pieColors) {
        int i = 0;
        for (PieChart.Data data : pieChartData) {
            data.getNode().setStyle("-fx-pie-color: " + pieColors[i % pieColors.length] + ";");
            i++;
        }
    }

    /**
     * метод для запуску програми
     * @param args системні параметри
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * клас для паралельного оновлення
     * інформації про загруженість RAM
     */
    class CountThread implements Runnable {
        /**
         * силка на обєкт з якого
         * можна дістати інформацію про пам*ять
         */
        private MemoryMonitor memory;
        /**
         * показчик прогрес бара загруженості RAM
         */
        private SimpleDoubleProperty value;
        /**
         * строковий показчик занятості RAM
         */
        private SimpleStringProperty str;

        /**
         * Конструктор класу
         * @param d початкова властивість для прогрес бару
         * @param s початкова властивість для строкового представлення
         */
        CountThread(SimpleDoubleProperty d,SimpleStringProperty s) {
            memory = new MemoryMonitor();
            value = d;
            str = s;
        }

        /**
         * мето для паралельного виконання
         */
        @Override
        public void run() {
           final double ramS = Double.parseDouble( memory.getRAMSize());
            while (true) {
               final double d = Double.parseDouble(memory.getRAMSize()) -
                                  Double.parseDouble(memory.getRamFree()) - 0.2;
               final String buf = new BigDecimal(d).setScale(1, RoundingMode.UP)+"";
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CountThread.class.getName()).log(Level.SEVERE, null, ex);
                }
               // System.out.println(d);
                if(d > 0) {
                    value.set(d/4.0);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            str.set("RAM used "+buf+" GB of " + ramS + " GB");
                        }
                    });
                }
            }
        }
    }

    /**
     * клас для паралельного оновлення
     * діаграми занятості процесора
     */
    class Changer implements Runnable {
        /**
         * силка на обєкт діаграми
         */
        private PieChart p;
        /**
         * силка на значення лейбла з процентним показчиком
         * занятості поцесора
         */
        private SimpleStringProperty sp;

        /**
         * Конструктор класу
         * @param pieChart оновлювана діаграма
         * @param s значення властивості лейбла
         */
        public Changer(PieChart pieChart,SimpleStringProperty s) {
            p = pieChart;
            sp = s;
        }

        /**
         * метод для паралельного виконання
         */
        @Override
        public void run() {
           // new Thread() {
              //  public void run() {
               //     while (true)
                      //  BigInteger.probablePrime(MAX_PRIORITY, new Random());
              //  }
           // }.start();
            p.getData().get(0).pieValueProperty().bind(processorValue);
            p.getData().get(1).pieValueProperty().bind(processorValue.multiply(-1).add(100));
            while (true) {
                try {
                    //final double d = 0.5*getMetric(sigar.getPid() + "") * getMetric() * 100-5;
                    final double d = getMetric()*100;
                    processorValue.set(d);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            double buf = d;
                            if( d > 100) buf = 90;
                            String s = buf+"";
                            int i = 0;
                            for (int j = 0; j < s.length(); j++) {
                                if(s.charAt(j) == '.') {
                                    i = j;
                                    break;
                                }
                            }
                            sp.setValue(s.substring(0,i)+"%");
                        }
                    });
                } catch (SigarException e) {
                    e.printStackTrace();
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * клас для оновлення таблиці з активними процесами
     * в паралельному потоці
     */
    class Refresher implements Runnable{
        /**
         * силка на головну панель вікна
         */
        private HBox box;
        /**
         * силка на головну панель сцени
         */
        private Group root;

        /**
         * конструктор класу
         * @param r силка на головну панель сцени
         * @param box силка на головну панель вікна
         */
        public Refresher(Group r,HBox box){
            this.box = box;
            this.root = r;
        }

        /**
         * метод для парелельного виконання
         */
        @Override
        public void run() {
            while (true) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        root.getChildren().remove(0);
                        box.getChildren().remove(1);
                        box.getChildren().add(new TablePanel().getBox(scene));
                        root.getChildren().add(box);
                    }
                });
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
