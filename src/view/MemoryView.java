package view;

import core.MemoryMonitor;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;

import java.util.HashMap;
import java.util.Map;

/**
 * Клас для створення
 * панелі з інфрмацією про
 * локальні диски
 * Created by Vlad on 11.12.2014.
 * @author Vlad
 */
public class MemoryView {
    /**
     * паель з інформацією про
     * локальні диски
     */
    private VBox box;
    /**
     * об*єкт який містить в собі інформацію про
     * пам*ять компютера
     */
    private MemoryMonitor memory;

    /**
     * конструктор класу
     */
    public MemoryView() {
        box = new VBox();
        memory = new MemoryMonitor();
    }

    /**
     * метод для створення панель з
     * інформацією про локальні диски
     * @return box  паель з інформацією про локальні диски
     */
    public VBox getBox(){
       box.setAlignment(Pos.BASELINE_CENTER);
       box.setSpacing(10);
       box.setPadding(new Insets(5));

        HashMap<String,String> map = memory.getDisksInfo();

        for (Map.Entry m: map.entrySet()){
            ProgressBar bar = new ProgressBar();
            bar.setMinWidth(160);
            String s = (String) m.getValue();
            int index = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ' ') {
                    index = i;
                    break;
                }
            }
            double free = Double.parseDouble(s.substring(0,index));
            double total =10*Double.parseDouble(s.substring(index + 1,s.length() - 1 ));
            SimpleDoubleProperty d = new SimpleDoubleProperty((total-free)/total);
            bar.progressProperty().bind(d);
            Label l = new Label("Disk("+m.getKey()+") "+free+"GB free of "+total+"GB");
            box.getChildren().addAll(l,bar);
        }

     return box;
    }

}
