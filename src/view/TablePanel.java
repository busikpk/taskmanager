package view;

import core.ListenerForProcess;
import core.TaskList;
import core.model.MyProcess;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import java.util.ArrayList;

/** клас для створення
 * таблиці з запущеними процесами
 * операційної системи
 * Created by Vlad on 13.12.2014.
 * @author Vlad
 */
public class TablePanel {
    /**
     * таблиця запущених процесів
     */
    private TableView table;
    /**
     * панель в якій знаходяться
     * таблиця та кнопка завершення процеса
     */
    private VBox mainBox;
    /**
     * силка на обєкт класу
     * для завершення вибраного процесу
     */
    private ListenerForProcess remover;

    /**
     * конструктор класу
     */
    public TablePanel(){
        table = new TableView();
        mainBox = new VBox();
        remover = new ListenerForProcess();
    }

    /**
     * мето для стаорення
     * панель з таблицею та
     * кнопкою для завершення
     * вибраного процесу
     * @param s силка на обєкт сцени потрібна
     *          для коректного відображення розмірів панелі
     * @return mainBox  панель з таблицею та
     *                  кнопкою для завершення
     */
    public VBox getBox(Scene s){
        mainBox.setPadding(new Insets(5));
        mainBox.setSpacing(10);

        Label label = new Label("Avalible processes");
        mainBox.setAlignment(Pos.TOP_LEFT);
        label.setFont(new Font("Tahoma",20));

        final ArrayList<MyProcess> buf = new ArrayList<MyProcess>();
        buf.add(new MyProcess("1","2","3"));
        buf.add(new MyProcess("2","1","3"));
        buf.add(new MyProcess("3","2","1"));
        buf.addAll(new TaskList().getProcesses());

        final ObservableList<MyProcess> processes = FXCollections.observableArrayList(
                buf);
        final TableView<MyProcess> processTableView = new TableView<MyProcess>();
        processTableView.prefWidthProperty().bind(s.widthProperty().subtract(240));
        processTableView.prefHeightProperty().bind(s.heightProperty().subtract(50));
        processTableView.setItems(processes);

        TableColumn<MyProcess, String> processNameCol = new TableColumn<MyProcess,String>("Process");
        processNameCol.setEditable(false);
        processNameCol.setCellValueFactory(new PropertyValueFactory<MyProcess, String>("name"));
        processNameCol.setPrefWidth(processTableView.getPrefWidth() / 3);

        TableColumn<MyProcess, String> pidNameCol = new TableColumn<MyProcess, String>("PID");
        pidNameCol.setCellValueFactory(new PropertyValueFactory<MyProcess, String>("PID"));
        pidNameCol.setPrefWidth(processTableView.getPrefWidth() / 3);

        TableColumn<MyProcess, String> memoryCol = new TableColumn<MyProcess,String>("Memory");
        memoryCol.setCellValueFactory(new PropertyValueFactory<MyProcess, String>("memory"));
        memoryCol.setPrefWidth(processTableView.getPrefWidth() / 3);

        processTableView.getColumns().setAll(processNameCol, pidNameCol, memoryCol);
        HBox bufBox = new HBox();
        bufBox.prefWidthProperty().bind(processTableView.widthProperty());
        bufBox.setAlignment(Pos.TOP_RIGHT);
        final Button endTask = new Button("End Task");
        bufBox.getChildren().add(endTask);

        endTask.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (buf.size() > 1) {
                            try {
                                String id = processTableView.getSelectionModel().getSelectedItem().getPID();
                                buf.remove(processTableView.getSelectionModel().getSelectedIndex());
                                //remover.destroyProcess(id);
                            } catch (Exception e) {
                                System.out.println("ne vudilenuy ryadok");
                            }
                            processes.clear();
                            processes.addAll(buf);
                        }
                    }
                });
                if (processes.size()==0) endTask.setDisable(true);
                else
                    endTask.setDisable(false);
            }
        });

        mainBox.getChildren().addAll(label, processTableView,bufBox);

        return mainBox;
    }

}
