package core;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * Клас для взяття інформації
 * про операційну систему
 * Created by Vlad on 12.12.2014.
 * @author Vlad
 */
public class WindowsInfo {
    /**
     * мапа з інформаціэю про операційну систему
     */
    private HashMap<String,String> map;

    /**
     * конструктор класу
     */
    public WindowsInfo(){
        map = new HashMap<String, String>();
    }

    /**
     * метод повертає мапу з інформацією про операційну систему
     * @return map мапа з інфрмацією про операційну систему
     */
    public HashMap<String,String> getWindowsInfo(){
        Properties properties = System.getProperties();

        Enumeration<String> prop =
                (Enumeration<String>) properties.propertyNames();

        while(prop.hasMoreElements()){
            String propName = prop.nextElement();
            if(propName.equals("os.arch")){
                map.put("arch",System.getProperty(propName));
                continue;
            }
            if(propName.equals("os.name")){
                map.put("os",System.getProperty(propName));
            }
        }

        return map;
    }
}
