package core.model;

/**
 * Клас для опису процесу який запущений
 * в операційній системі
 * Created by Vlad on 08.12.2014.
 * @author Vlad
 */
public class MyProcess {
    /**
     * ім*я процесу
     */
    private  String name;
    /**
     * унікальний ідентифікатро процесу
     */
    private String PID;
    /**
     * пам*ять яку займає процес
     */
    private String memory;

    /**
     * Конструктор класу
     * @param name ім*я
     * @param PID ідентифікатор
     * @param memory займаєма память
     */
    public MyProcess(String name,String PID, String memory){
        this.name = name;
        this.PID = PID;
        this.memory = memory;
    }

    /**
     * метод вертає ім*я процеса
     * @return name ім*я
     */
    public String getName() {
        return name;
    }

    /**
     * метод встановлює ім*я процеса
     * @param name  ім*я
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * метод вертає ІД процеса
     * @return PID ІД
     */
    public String getPID() {
        return PID;
    }

    /**
     * метод встановлює унікальний
     * ідентифікатор для просеса
     * @param PID ІД
     */
    public void setPID(String PID) {
        this.PID = PID;
    }

    /**
     * метод повертає занімаєму пам*ять процесом
     * @return memory пам*ять
     */
    public String getMemory() {
        return memory;
    }

    /**
     * мето встановлює занімаєму пам*ять процесом
     * @param memory пам*ять
     */
    public void setMemory(String memory) {
        this.memory = memory;
    }

    /**
     * метод для строкового представлення
     * активного процесу
     * @return строкове представлення процесу
     */
    public String toString(){
        return "name: "+name+" PID:\t "+PID+" memory:\t "+memory;
    }
}
