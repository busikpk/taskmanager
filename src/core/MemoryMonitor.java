package core;

import com.sun.management.OperatingSystemMXBean;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;

/**Клас для отримання інформацію про стан
 * пам*яті
 * Created by Vlad on 11.12.2014.
 * @author Vlad
 */
public class MemoryMonitor {
    /**
     * метод повертає загальний розмір
     * оперативної пам*яті в операційній системі
     * @return RAM size
     */
    public String getRAMSize() {
        String s="";
        OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);
            if (method.getName().startsWith("getTotalPhysicalMemorySize")
                    && Modifier.isPublic(method.getModifiers())) {
                Object value;
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                }
                s=""+Double.parseDouble(value.toString())/(1024*1024*1024);
            }
        }
    return  String.valueOf(Double.parseDouble(s.substring(0,3))+0.1);
    }

    /**
     * метод повертає значення
     * вільної оперативної пам*ятьі
     * @return sb кількість вільних гігабайт оперативної пам*яті
     */
    public String getRamFree(){
        com.sun.management.OperatingSystemMXBean operatingSystemMXBean =
                (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
        com.sun.management.OperatingSystemMXBean os = (com.sun.management.OperatingSystemMXBean)
                java.lang.management.ManagementFactory.getOperatingSystemMXBean();
        long physicalfreeMemorySize = os.getFreePhysicalMemorySize();
        String s = String.valueOf(physicalfreeMemorySize).substring(0,3);
        StringBuffer sb = new StringBuffer();
        sb.append(s.charAt(0));
        sb.append(".");
        sb.append(s.charAt(2));
      return sb.toString();
    }

    /**
     * метод повертає інформацію про жосткі диски
     * @return map мапа інформації про диски які є в операційній системі
     */
    public HashMap<String,String> getDisksInfo(){
        int gb = 1024*1024*1024;
        File root[] = File.listRoots();
        ArrayList<String> disks = new ArrayList<String>();

        for (int i = 0; i < root.length; i++) {
            disks.add(root[i].getAbsolutePath().substring(0, root[i].getAbsolutePath().length() - 1));
        }

        HashMap<String,String> map = new HashMap<String, String>();

        for (int i = 0; i < disks.size(); i++) {
            File diskInfo = new File(disks.get(i));
            if (diskInfo.getTotalSpace() == 0) continue;
            else {
                String s = "";
                s += diskInfo.getFreeSpace()/gb + " " + diskInfo.getTotalSpace()/gb;
                map.put(disks.get(i),s);
            }
        }
        return  map;
    }

}
