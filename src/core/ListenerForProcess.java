package core;

import java.io.IOException;

/**Клас для можливості закриття процеса
 * в операційній системі Windows
 * Created by Vlad on 08.12.2014.
 * @author Vlad
 */
public class ListenerForProcess {
    /**
     * процес завершення процесу
     */
    private Process process;

    /**
     * конструктор класу
     */
    public ListenerForProcess(){
    }

    /**
     * метод для принудітєльного завершення процесу
     * в операційній системі Windows
     * @param name ІД процесу для завершення
     */
    public void destroyProcess(String name){
        try {
            process = Runtime.getRuntime().exec("taskkill /pid "+name+" /f");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
