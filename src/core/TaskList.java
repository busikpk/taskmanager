package core;

import core.model.MyProcess;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Клас для обробки запущених
 * процесів в операційній системі
 * Windows
 * Created by Vlad on 08.12.2014.
 * @author Vlad
 */
public class TaskList {
    /**
     * масив запущених процесів
     */
    private ArrayList<MyProcess> processes;

    /**
     * конструктор класу
     */
    public TaskList(){
        processes = new ArrayList<MyProcess>();
        getRuningProcesses();
    }

    /**
     * метод для заповнення масиву processes значеннями
     * метод знаходить всі зарущені процеси і записує
     * їх в масив
     */
    private void getRuningProcesses(){
        String line;
        try {
            Process p = Runtime.getRuntime().exec("tasklist.exe /nh");
            BufferedReader input = new BufferedReader
                    (new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if (!line.trim().equals("")) {
                    line = line.replaceAll("�","");
                    processes.add(new MyProcess(
                            getNameOfProcess(line),
                            getPIDofProces(line),
                            getMemoryOfProcess(line)));
                }
            }
            if(processes.size() > 2) processes.remove(0);
            input.close();
        }
        catch (Exception err) {
            err.printStackTrace();
        }
        System.out.println(processes.size());
    }

    /**
     * метод для знаходження імені запущеного процесу
     * @param line - строка з tasklist.exe
     * @return ім*я процесу
     */
    private String getNameOfProcess(String line){
        int end = 0;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == ' ') {
                end = i;
                break;
            }
        }
        return line.substring(0,end);
    }
    /**
     * метод для знаходження ІД запущеного процесу
     * @param line - строка з tasklist.exe
     * @return ІД процесу
     */
    private String getPIDofProces(String line){
        int start = 0;
        int end = 0;
        for (int i = 0; i < line.length()-1; i++) {
            if(line.charAt(i) == ' '){
                if (line.charAt( i + 1) !=' '){
                    start = i + 1;
                    int index = i + 1;
                    while (line.charAt(index++) != ' ')
                        end = index;
                    break;
                }
            }
        }
        return line.substring(start,end);
    }
    /**
     * метод для знаходження займаємої пам*яті запущеного процесу
     * @param line - строка з tasklist.exe
     * @return  занята пам*ять
     */
    private String getMemoryOfProcess(String line){
        int start = 0;
        int k = 0;
        for (int i = line.length() - 1; i >= 0 ; i--) {
           if (k > 1){
               start = i;
               break;
           }
            if (line.charAt(i) == ' ') k++;
        }
        return line.substring(start, line.length() - 1);
    }

    /**
     * масив запущених просесів
     * @return  processes масив запущених процесів
     */
    public ArrayList<MyProcess> getProcesses(){
        return processes;
    }
}
